<div class="box box-element ui-draggable">
    <a href="#close" class="remove label label-important">
        <i class="icon-remove icon-white"></i>Remove</a>
    <span class="drag label"><i class="icon-move"></i>Drag</span>
    <div class="preview">AddEndereco</div>
    <div class="view">
        <form action="" method="post" name="formEndereco" class="classForm" id="formEndereco" novalidate="novalidate">
            <fieldset>
                <h3 style="margin-top: -3%">Endereço</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div style="float: left; margin-right: 5px;" class="form-group">
                            <label for="co_cep" class="block">CEP&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <input type="text" value="" tabindex="29" class="form-control" style="float: left; width: 130px; " title="CEP" required="required" placeholder="Entre com o CEP" id="co_cep" name="co_cep">
                            <script type="text/javascript">$(document).ready(function () { include_once("/vendor/jquery-ui/jquery-ui-1.11.4/js/jquery-ui.min.js.php");
                                    include_once("/vendor/jquery-base64/jquery-base64-0.1/jquery.base64.js.php");
                                });</script><script type="text/javascript">include_once("/vendor/jquery-maskedinput/jquery-maskedinput-1.3.1/jquery.maskedinput.js.php");</script><script type="text/javascript">$("#co_cep").mask("99999-999");</script>
                        </div>
                        <div class="divHeightRow linkCorreios">
                            <a title="Abre a página dos Correios." target="_blank" href="http://www.correios.com.br">Consulte seu CEP</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div style="float: left; margin-right: 5px; width: 60%;" class="form-group">
                            <label for="no_logradouro" class="block">Logradouro&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <input type="text" value="" tabindex="30" class="form-control" maxlength="100" data-domain="co_cep" style="float: left;" title="Logradouro" required="required" placeholder="Entre com o Logradouro" id="no_logradouro" name="no_logradouro">
                        </div>
                        <div style="display: block; overflow: hidden;" class="form-group">
                            <label for="nu_endereco" class="block">Número&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <input type="text" value="" tabindex="31" class="form-control" maxlength="10" data-domain="co_cep" title="Número" required="required" placeholder="Entre com o Número" id="nu_endereco" name="nu_endereco">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div style="float: left; margin-right: 5px; width: 60%;" class="form-group"><label for="ds_complemento" class="block">Complemento</label>
                            <input type="text" value="" tabindex="32" class="form-control valid" maxlength="80" data-domain="co_cep" style="float: left;" title="Complemento" placeholder="Entre com o Complemento" id="ds_complemento" name="ds_complemento">
                        </div>
                        <div style="display: block; overflow: hidden;" class="form-group"><label for="no_bairro" class="block">Bairro&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <input type="text" value="" tabindex="33" class="form-control" maxlength="100" data-domain="co_cep" title="Bairro" required="required" placeholder="Entre com o Bairro" id="no_bairro" name="no_bairro">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div style="float: left; margin-right: 5px; width: 25%;" class="form-group"><label for="co_uf_endereco" class="block">UF&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <select tabindex="34" class="form-control" onchange="feedSelect('co_uf_endereco', 'co_municipio_endereco', '/corp_municipio/ajaxFetchPairs', undefined, true);" data-depend="co_municipio_endereco" data-domain="co_cep" style="float: left;" title="UF" required="required" id="co_uf_endereco" name="co_uf_endereco">
                                <option value="">Selecione</option><option value="12">Acre</option><option value="27">Alagoas</option><option value="13">Amazonas</option><option value="16">Amapá</option><option value="29">Bahia</option><option value="23">Ceará</option><option value="53">Distrito Federal</option><option value="32">Espírito Santo</option><option value="52">Goiás</option><option value="21">Maranhão</option><option value="31">Minas Gerais</option><option value="50">Mato Grosso do Sul</option><option value="51">Mato Grosso</option><option value="15">Pará</option><option value="25">Paraíba</option><option value="26">Pernambuco</option><option value="22">Piauí</option><option value="41">Paraná</option><option value="33">Rio de Janeiro</option><option value="24">Rio Grande do Norte</option><option value="11">Rondônia</option><option value="14">Roraima</option><option value="43">Rio Grande do Sul</option><option value="42">Santa Catarina</option><option value="28">Sergipe</option><option value="35">São Paulo</option><option value="17">Tocantins</option>
                            </select>
                        </div>
                        <div style="display: block; overflow: hidden;" class="form-group"><label for="co_municipio_endereco" class="block">Município&nbsp;<i style="color: #9A0000" class="fa fa-asterisk"><div class="clearfix"></div></i></label>
                            <select tabindex="35" class="form-control" data-domain="co_cep" title="Município" required="required" id="co_municipio_endereco" name="co_municipio_endereco">
                                <option value="">Selecione</option><option value="1200013">Acrelândia</option><option value="1200054">Assis Brasil</option><option value="1200104">Brasiléia</option><option value="1200138">Bujari</option><option value="1200179">Capixaba</option><option value="1200203">Cruzeiro do Sul</option><option value="1200252">Epitaciolândia</option><option value="1200302">Feijó</option><option value="1200328">Jordão</option><option value="1200336">Mâncio Lima</option><option value="1200344">Manoel Urbano</option><option value="1200351">Marechal Thaumaturgo</option><option value="1200385">Plácido de Castro</option><option value="1200807">Porto Acre</option><option value="1200393">Porto Walter</option><option value="1200401">Rio Branco</option><option value="1200427">Rodrigues Alves</option><option value="1200435">Santa Rosa do Purus</option><option value="1200500">Sena Madureira</option><option value="1200450">Senador Guiomard</option><option value="1200609">Tarauacá</option><option value="1200708">Xapuri</option>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

