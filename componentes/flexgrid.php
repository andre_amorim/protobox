<div class="box box-element ui-draggable">
    <a href="#close" class="remove label label-important">
    <i class="icon-remove icon-white"></i>Remove</a>
    <span class="drag label"><i class="icon-move"></i>Drag</span>
    <span class="configuration">
        <button type="button" class="btn btn-mini" rel="flegrid-addcol">Add Coluna</button>
    </span>
    <div class="preview">FlexGrid</div>
    <div class="view">
        <div class="panel panel-info">
            <div class="flexigrid">
                <div class="nBtn" title="Hide/Show Columns" style="top: 2px; display: none;">
                    <div></div>
                </div>
                <div class="nDiv" unselectable="on" style="margin-bottom: -310px; display: none; top: 31px; height: auto; width:auto;">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td class="ndcol1">
                                <input type="checkbox" checked="checked" class="togCol" value="0"></td>
                            <td class="ndcol2">Nº</td>
                        </tr>
                        <tr>
                            <td class="ndcol1"><input type="checkbox" checked="checked" class="togCol" value="1"></td>
                            <td class="ndcol2">Data de Envio</td>
                        </tr>
                        <tr>
                            <td class="ndcol1"><input type="checkbox" checked="checked" class="togCol" value="2"></td>
                            <td class="ndcol2">Enviado Por</td>
                        </tr>
                        <tr>
                            <td class="ndcol1"><input type="checkbox" checked="checked" class="togCol" value="3"></td>
                            <td class="ndcol2">Projeto</td>
                        </tr>
                        <tr>
                            <td class="ndcol1"><input type="checkbox" checked="checked" class="togCol" value="4"></td>
                            <td class="ndcol2">Assunto do Comunicado</td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div class="tDiv">
                    <div class="tDiv2"></div>
                    <div style="clear:both"></div>
                </div>
                <div class="hDiv">
                    <div class="hDivBox">
                    <table cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th axis="col0" abbr="ROWNUM" align="left" class="">
                                <div class="" style="text-align: left; width: 50px;" contenteditable="true">Nº</div>
                            </th>
                            <th axis="col1" abbr="DT_INCLUSAO" align="left">
                                <div style="text-align: left; width: 200px;" contenteditable="true">Data de Envio</div>
                            </th>
                            <th axis="col2" abbr="NO_USUARIO" align="left" class="">
                                <div class="" style="text-align: left; width: 200px;" contenteditable="true">Enviado Por</div>
                            </th>
                            <th axis="col2" abbr="NO_PROJETO" align="left" class="">
                                <div class="" style="text-align: left; width: 250px;" contenteditable="true">Projeto</div>
                            </th>
                            <th axis="col2" abbr="NO_ASSUNTO" align="left" class="">
                                <div class="" style="text-align: left; width: 300px;" contenteditable="true">Assunto do Comunicado
                                </div>
                            </th>
                            <th axis="col1" abbr="ACOES" align="left" class="">
                                <div class="" style="text-align: left; width: 50px;">Ações</div>
                            </th>
                        </tr>
                        </thead>
                    </table>
                </div>
                </div>
                <div class="cDrag" style="top: 3px;">
                    <div style="height: 339px; display: block;"></div>
                    <div style="height: 339px; display: block;"></div>
                    <div style="height: 339px; display: block;"></div>
                    <div style="height: 339px; display: block;"></div>
                    <div style="height: 339px; display: block;"></div>
                </div>
                <div class="bDiv" style="height: auto; min-width: 0;">
                    <table summary="flexigridTable" id="tableLogComunicadoInicial" style="" cellpadding="0" cellspacing="0" border="0">
                    <tbody>
                        <tr id="row2161">
                            <td align="left" abbr="ROWNUM">
                                <div style="text-align: left; width: 50px;" contenteditable="true">1</div>
                            </td>
                            <td align="left" abbr="DT_INCLUSAO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">06/07/2015</div>
                            </td>
                            <td align="left" abbr="NO_USUARIO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">ANDRE NEVES DE AMORIM</div>
                            </td>
                            <td align="left" abbr="NO_PROJETO">
                                <div style="text-align: left; width: 250px;" contenteditable="true">BRASIL</div>
                            </td>
                            <td align="left" abbr="NO_ASSUNTO">
                                <div style="text-align: left; width: 300px;" contenteditable="true">Primeiro comunicado</div>
                            </td>
                            <td align="left" abbr="ACOES">
                                <div style="text-align: left; width: 50px;">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                    <i class="fa fa-trash" title="Excluir"></i>
                                </div>
                            </td>
                        </tr>
                        <tr id="row2161">
                            <td align="left" abbr="ROWNUM">
                                <div style="text-align: left; width: 50px;" contenteditable="true">2</div>
                            </td>
                            <td align="left" abbr="DT_INCLUSAO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">06/07/2015</div>
                            </td>
                            <td align="left" abbr="NO_USUARIO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">JEFERSON DA SILVA</div>
                            </td>
                            <td align="left" abbr="NO_PROJETO">
                                <div style="text-align: left; width: 250px;" contenteditable="true">PORTUGUAL</div>
                            </td>
                            <td align="left" abbr="NO_ASSUNTO">
                                <div style="text-align: left; width: 300px;" contenteditable="true">Primeiro comunicado</div>
                            </td>
                            <td align="left" abbr="ACOES">
                                <div style="text-align: left; width: 50px;">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                    <i class="fa fa-trash" title="Excluir"></i>
                                </div>
                            </td>
                        </tr>
                        <tr id="row2161">
                            <td align="left" abbr="ROWNUM">
                                <div style="text-align: left; width: 50px;" contenteditable="true">3</div>
                            </td>
                            <td align="left" abbr="DT_INCLUSAO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">02/08/2015</div>
                            </td>
                            <td align="left" abbr="NO_USUARIO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">ALEXANDRE DY LA FLUENTE</div>
                            </td>
                            <td align="left" abbr="NO_PROJETO">
                                <div style="text-align: left; width: 250px;" contenteditable="true">PARAGUAI</div>
                            </td>
                            <td align="left" abbr="NO_ASSUNTO">
                                <div style="text-align: left; width: 300px;" contenteditable="true">Segundo comunicado</div>
                            </td>
                            <td align="left" abbr="ACOES">
                                <div style="text-align: left; width: 50px;">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                    <i class="fa fa-trash" title="Excluir"></i>
                                </div>
                            </td>
                        </tr>
                        <tr id="row2161">
                            <td align="left" abbr="ROWNUM">
                                <div style="text-align: left; width: 50px;" contenteditable="true">4</div>
                            </td>
                            <td align="left" abbr="DT_INCLUSAO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">02/08/2015</div>
                            </td>
                            <td align="left" abbr="NO_USUARIO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">PAULO GOMES</div>
                            </td>
                            <td align="left" abbr="NO_PROJETO">
                                <div style="text-align: left; width: 250px;" contenteditable="true">MEXICO</div>
                            </td>
                            <td align="left" abbr="NO_ASSUNTO">
                                <div style="text-align: left; width: 300px;" contenteditable="true">Segundo comunicado</div>
                            </td>
                            <td align="left" abbr="ACOES">
                                <div style="text-align: left; width: 50px;">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                    <i class="fa fa-trash" title="Excluir"></i>
                                </div>
                            </td>
                        </tr>
                        <tr id="row2161">
                            <td align="left" abbr="ROWNUM">
                                <div style="text-align: left; width: 50px;" contenteditable="true">5</div>
                            </td>
                            <td align="left" abbr="DT_INCLUSAO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">14/08/2015</div>
                            </td>
                            <td align="left" abbr="NO_USUARIO">
                                <div style="text-align: left; width: 200px;" contenteditable="true">RAFAELA SOBRAL</div>
                            </td>
                            <td align="left" abbr="NO_PROJETO">
                                <div style="text-align: left; width: 250px;" contenteditable="true">ARGENTINA</div>
                            </td>
                            <td align="left" abbr="NO_ASSUNTO">
                                <div style="text-align: left; width: 300px;" contenteditable="true">Terceiro comunicado</div>
                            </td>
                            <td align="left" abbr="ACOES">
                                <div style="text-align: left; width: 50px;">
                                    <i class="fa fa-pencil" title="Editar"></i>
                                    <i class="fa fa-trash" title="Excluir"></i>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                    <div class="iDiv" style="display: none;"></div>
                </div>
                <div class="pDiv">
                    <div class="pDiv2">
                    <div class="pGroup"><select name="rp">
                            <option value="10" selected="selected">10&nbsp;&nbsp;</option>
                            <option value="15">15&nbsp;&nbsp;</option>
                            <option value="20">20&nbsp;&nbsp;</option>
                            <option value="30">30&nbsp;&nbsp;</option>
                            <option value="50">50&nbsp;&nbsp;</option>
                        </select>
                    </div>
                    <div class="btnseparator"></div>
                    <div class="pGroup">
                        <div class="pFirst pButton"><i class="fa fa fa-fast-backward"></i>&nbsp;</div>
                        <div class="pPrev pButton"><i class="fa fa-step-backward"></i>&nbsp;</div>
                    </div>
                    <div class="btnseparator"></div>
                    <div class="pGroup">
                        <span class="pcontrol">Página
                            <input type="text" size="4" value="1" name="page_grid"> de <span>1</span>
                        </span>
                    </div>
                    <div class="btnseparator"></div>
                    <div class="pGroup">
                        <div class="pNext pButton"><i class="fa fa-step-forward"></i>&nbsp;</div>
                        <div class="pLast pButton"><i class="fa fa-fast-forward"></i>&nbsp;</div>
                    </div>
                    <div class="btnseparator"></div>
                    <div class="pGroup">
                        <div class="pReload pButton"><i class="fa fa-repeat"></i>&nbsp;</div>
                    </div>
                    <div class="btnseparator"></div>
                    <div class="pGroup"><span class="pPageStat">Listando 1 até 1 de 1 registro(s)</span></div>
                </div>
                    <div style="clear:both"></div>
                </div>
                <div class="vGrip"><span></span></div>
            </div>
        </div>
    </div>
</div>
