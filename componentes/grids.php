<div class="sidebar-nav">
    <ul class="nav nav-list accordion-group">
        <li class="nav-header">
            <div class="pull-right popover-info"><i class="icon-question-sign "></i>
                <div class="popover fade right">
                    <div class="arrow"></div>
                    <h3 class="popover-title">Help</h3>
                    <div class="popover-content">Help</div>
                </div>
            </div>
            <i class="icon-plus icon-white"></i> GRID </li>
        <li style="display: list-item;" class="rows" id="estRows">
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="12" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span12 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="6 6" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span6 column"></div>
                        <div class="span6 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="8 4" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span8 column"></div>
                        <div class="span4 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="4 4 4" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span4 column"></div>
                        <div class="span4 column"></div>
                        <div class="span4 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="2 6 4" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span2 column"></div>
                        <div class="span6 column"></div>
                        <div class="span4 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="6 4 4" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="col-xs-6 span4 column"></div>
                        <div class="col-xs-6 span4 column"></div>
                        <div class="col-xs-6 span4 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="9 3" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="col-xs-6 span9 column" ></div>
                        <div class="col-xs-6 span3 column "></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="3 3 3 3" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="col-xs-3 span3 column" ></div>
                        <div class="col-xs-3 span3 column "></div>
                        <div class="col-xs-3 span3 column"></div>
                        <div class="col-xs-3 span3 column"></div>
                    </div>
                </div>
            </div>
            <div class="lyrow ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
                <div class="preview">
                    <input value="Nested Columns" type="text">
                </div>
                <div class="view">
                    <div class="row-fluid clearfix">
                        <div class="span12 column">
                            <div class="row-fluid clearfix">
                                <div class="span6 column">

                                </div>
                                <div class="span6 column">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
