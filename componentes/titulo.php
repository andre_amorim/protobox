<div class="box box-element ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span>
            	 <span class="configuration"><button type="button" class="btn btn-mini" data-target="#editorModal" role="button" data-toggle="modal">Editor</button> <span class="btn-group"> <a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Align <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li class="active"><a href="#" rel="">Default</a></li>
                  <li class=""><a href="#" rel="text-left">Left</a></li>
                  <li class=""><a href="#" rel="text-center">Center</a></li>
                  <li class=""><a href="#" rel="text-right">Right</a></li>
              </ul>
              </span> <span class="btn-group"> <a class="btn btn-mini dropdown-toggle" data-toggle="dropdown" href="#">Emphasis <span class="caret"></span></a>
              <ul class="dropdown-menu">
                  <li class="active"><a href="#" rel="">Default</a></li>
                  <li class=""><a href="#" rel="muted">Muted</a></li>
                  <li class=""><a href="#" rel="text-warning">Warning</a></li>
                  <li class=""><a href="#" rel="text-error">Error</a></li>
                  <li class=""><a href="#" rel="text-info">Info</a></li>
                  <li class=""><a href="#" rel="text-success">Success</a></li>
              </ul>
              </span> </span>
    <div class="preview">Titulo</div>
    <div class="view">
        <h3 contenteditable="true">Titulo da página</h3>
    </div>
</div>
