<div class="box box-element ui-draggable"> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span> <span class="configuration"><button type="button" class="btn btn-mini" data-target="#editorModal" role="button" data-toggle="modal">Editor</button> <a class="btn btn-mini" href="#" rel="well">Well</a> </span>
    <div class="preview">Welcome</div>
    <div class="view">
        <div class="hero-unit" contenteditable="true">
            <h1>Tela de bem vindo!</h1>
            <p>This is a template for a simple marketing or information website.
                It includes a large callout called the herop unit and three  supporting pieaces of content. Use iot as starting point to create something more unique</p>
            <p><a class="btn btn-primary btn-large" href="#">Learn More »</a></p>
        </div>
    </div>
</div>
