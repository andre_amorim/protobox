<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="title" content="Create page with Bootstrap">
<meta name="description" content="Create page with Bootstrap">
<meta name="keywords" content="Create page with Bootstrap">
<title>Protobox</title>

<!-- Le styles -->
<link href="css/bootstrap-combined.min.css" rel="stylesheet">
<link href="css/layoutit.css" rel="stylesheet">
<link href="css/docs.min.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
	<![endif]-->

	<!-- Fav and touch icons -->
	<link rel="shortcut icon" href="img/favicon.png">

	<script type="text/javascript" src="js/jquery-2.0.0.min.js"></script>
	<!--[if lt IE 9]>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="js/jquery.htmlClean.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor/config.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/FileSaver.js"></script>
<script type="text/javascript" src="js/blob.js"></script>
<script src="js/docs.min.js"></script>

<style>

</style>
</head>

<body style="min-height: 660px; cursor: auto;" class="edit">
<div class="navbar navbar-inner navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container-fluid">
      <div class="nav-collapse collapse">
      	<ul class="nav" id="menu-layoutit">
          <li class="divider-vertical">
              <button class="btn btn-primary">PROTOBOX</button>
          </li>
          <li>
		  <div class="btn-group">
			<button onclick="resizeCanvas('lg')" class="btn btn-default"><i class="fa fa-desktop"></i> </button>
			<button onclick="resizeCanvas('md')" class="btn btn-default"><i class="fa fa-laptop"></i> </button>
			<button onclick="resizeCanvas('sm')" class="btn btn-default"><i class="fa fa-tablet"></i> </button>
			<button onclick="resizeCanvas('xs')" class="btn btn-default"><i class="fa fa-mobile-phone"></i> </button>
		  </div>
            <div class="btn-group" data-toggle="buttons-radio">
              <button type="button" id="edit" class="btn btn-inverse active"><i class="icon-edit icon-white"></i> Edição
              </button>
              <button type="button" class="btn btn-inverse" id="devpreview"><i class="icon-eye-close icon-white">
              </i> Desenvolvedor</button>
              <button type="button" class="btn btn-inverse" id="sourcepreview"><i
                      class="icon-eye-open icon-white"></i> Visualização</button>
            </div>
            <button type="button" class="btn btn-inverse" data-target="#downloadModal" rel="/build/downloadModal"
                  role="button" data-toggle="modal"><i class="icon-chevron-down icon-white"></i> Download</button>
            <button class="btn btn-danger" href="#clear" id="clear"><i class="icon-trash icon-white"></i> Limpar</button>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>
</div>
<div class="container-fluid">
<div class="changeDimension">
  <div class="row-fluid">
    <div class="">
        <?php include 'componentes/grids.php' ?>
        <ul class="nav nav-list accordion-group">
          <li class="nav-header"><i class="icon-plus icon-white"></i> BASICOS
            <div class="pull-right popover-info"><i class="icon-question-sign "></i>
              <div class="popover fade right">
                <div class="arrow"></div>
                <h3 class="popover-title">Help</h3>
                <div class="popover-content">Help</div>
              </div>
            </div>
          </li>
          <li style="display: none;" class="boxes" id="elmBase">
            <?php include 'componentes/titulo.php' ?>
            <?php include 'componentes/paragrafo.php' ?>
            <?php include 'componentes/cabecalho.php'; ?>
            <?php include 'componentes/texto.php'; ?>
            <?php include 'componentes/citacao.php' ?>
            <?php include 'componentes/lista-nao-ordenada.php' ?>
            <?php include 'componentes/lista-ordenada.php' ?>
            <?php include 'componentes/descricao.php' ?>
            <?php include 'componentes/botao.php' ?>
            <?php include 'componentes/label.php' ?>
            <?php include 'componentes/badge.php' ?>
          </li>
        </ul>
        <ul class="nav nav-list accordion-group">
          <li class="nav-header"><i class="icon-plus icon-white"></i> COMPONENTES
            <div class="pull-right popover-info"><i class="icon-question-sign "></i>
              <div class="popover fade right">
                <div class="arrow"></div>
                <h3 class="popover-title">Help</h3>
                <div class="popover-content"> Help</div>
              </div>
            </div>
          </li>
          <li style="display: none;" class="boxes" id="elmComponents">
            <?php include 'componentes/flexgrid.php'; ?>
            <?php include 'componentes/welcome.php'; ?>
            <?php include 'componentes/threebox.php'; ?>
            <?php include 'componentes/media-object.php'; ?>
            <?php include 'componentes/modal.php'; ?>
            <?php include 'componentes/alerts.php'; ?>
          </li>
        </ul>
        <ul class="nav nav-list accordion-group">
            <li class="nav-header"><i class="icon-plus icon-white"></i> FOMULARIOS
                <div class="pull-right popover-info"><i class="icon-question-sign "></i>
                    <div class="popover fade right">
                        <div class="arrow"></div>
                        <h3 class="popover-title">Help</h3>
                        <div class="popover-content"> Help</div>
                    </div>
                </div>
            </li>
            <li style="display: none;" class="boxes" id="elmComponents">
                <?php include 'componentes/addEndereco.php'; ?>
            </li>
        </ul>
      </div>
    </div>
    <!--/span-->
    <div class="demo ui-sortable" style="min-height: 304px; ">

      <div class="lyrow">
  <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>remove</a>
  <span class="drag label"><i class="icon-move"></i>drag</span>
  <div class="preview">9 3</div>
  <div class="view">
    <div class="row-fluid clearfix">
      <div class="span12 column ui-sortable">

        <div class="box box-element ui-draggable" style="display: block; "> <a href="#close" class="remove label label-important"><i class="icon-remove icon-white"></i>Remove</a> <span class="drag label"><i class="icon-move"></i>Drag</span> <span class="configuration"><button type="button" class="btn btn-mini" data-target="#editorModal" role="button" data-toggle="modal">Editor</button> <a class="btn btn-mini" href="#" rel="well">Well</a> </span>
              <div class="preview">Jumbotron</div>
              <div class="view">
                <div class="hero-unit" contenteditable="true">
                        <h1>Hello, world!</h1>
						<p>This is a template for a simple marketing or informational website.</p>
						<p> It includes a large callout called the hero unit and three supporting pieces of content.</p>
						Use it as a starting point to create something more unique.</p>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>
</div>



    </div>
    <!-- end demo -->
    <!--/span-->

    <div id="download-layout">
      <div class="container-fluid"></div>
    </div>
  </div>
  <!--/row-->
</div>
<!--/.fluid-container-->
<div class="modal hide fade" role="dialog" id="editorModal">
  <div class="modal-header"> <a class="close" data-dismiss="modal">×</a>
    <h3>Save your Layout</h3>
  </div>
  <div class="modal-body">
    <p>
      <textarea id="contenteditor"></textarea>
    </p>
  </div>
  <div class="modal-footer"> <a id="savecontent" class="btn btn-primary" data-dismiss="modal">Save</a> <a class="btn" data-dismiss="modal">Cancel</a> </div>
</div>
<div class="modal hide fade" role="dialog" id="downloadModal">
  <div class="modal-header"> <a class="close" data-dismiss="modal">×</a>
    <h3>Save</h3>
  </div>
  <div class="modal-body">
    <p>Choose how to save your layout</p>
    <div class="btn-group">
      <button type="button" id="fluidPage" class="active btn btn-info"><i class="icon-fullscreen icon-white"></i> Fluid Page</button>
      <button type="button" class="btn btn-info" id="fixedPage"><i class="icon-screenshot icon-white"></i> Fixed page</button>
    </div>
    <br>
    <br>
    <p>
      <textarea></textarea>
    </p>
  </div>
  <div class="modal-footer"> <a class="btn" data-dismiss="modal" onclick="javascript:saveHtml();">Save</a> </div>
</div>
</div>
<script>
function resizeCanvas(size)
{

var containerID = document.getElementsByClassName("changeDimension");
var containerDownload = document.getElementById("download-layout").getElementsByClassName("container-fluid")[0];
var row = document.getElementsByClassName("demo ui-sortable");
var container1 = document.getElementsByClassName("container1");
if (size == "md")
{
$(containerID).width('id', "MD");
$(row).attr('id', "MD");
$(container1).attr('id', "MD");
$(containerDownload).attr('id', "MD");
}
if (size == "lg")
{
$(containerID).attr('id', "LG");
$(row).attr('id', "LG");
$(container1).attr('id', "LG");
$(containerDownload).attr('id', "LG");
}
if (size == "sm")
{
$(containerID).attr('id', "SM");
$(row).attr('id', "SM");
$(container1).attr('id', "SM");
$(containerDownload).attr('id', "SM");
}
if (size == "xs")
{
$(containerID).attr('id', "XS");
$(row).attr('id', "XS");
$(container1).attr('id', "XS");
$(containerDownload).attr('id', "XS");

}


}
</script>

    <!-- FLEXGRID -->
    <link href="vendor/flexigrid/flexigrid-1.1/css/flexigrid.pack.css.php" media="screen" rel="stylesheet" type="text/css">
    <link href="vendor/flexigrid/flexigrid-1.1/css/inep-zend.min.css.php" media="screen" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="vendor/flexigrid/flexigrid-1.1/js/flexigrid.js.php"></script>
    <script type="text/javascript" src="vendor/flexigrid/flexigrid-1.1/js/inep-zend.min.js.php"></script>
    <!-- FIM FLEXGRID -->
</body>
</html>
